import igraph as ig
import numpy as np
import os.path
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from scipy.optimize import curve_fit
from matplotlib.ticker import FuncFormatter


import tools

lib = 'nx'

samples = [(30, 1), (30, 6)]

n_launch = 100000

under_plot = plt.subplot(2, 1, 2)
def to_percent(x, y):
    return "%i" % int(100*x) + '%'

under_plot.yaxis.set_major_formatter(FuncFormatter(to_percent))

for sample, (n, m) in enumerate(samples):
    _, degrees = tools.get_empirical_law(n, m, n_launch=n_launch)

    _, degrees_law = tools.get_biased_law(n, m)

    print "empirical degrees\t", degrees
    print "law degree\t\t\t", degrees_law

    ax1 = plt.subplot(2, 2, sample + 1)
    ax1.set_title("n=%i, m=%i" % (n,m))
    ax1.plot(degrees, ls="_", marker="o", label="degrees")
    ax1.plot(degrees_law, ls="--", label="estimation")
    ax1.set_ylim([0, int(max(degrees)) + 2])
    ax1.set_ylabel("degree")
    ax1.set_xlabel("node index")
    ax1.legend()

    under_plot.plot(np.divide(degrees_law - degrees, degrees), label="n=%i, m=%i" % (n,m))

under_plot.legend()
under_plot.set_title("relative errors")
under_plot.set_ylabel("relative error")
under_plot.set_xlabel("node index")

plt.subplots_adjust(hspace=0.4)
plt.show()

