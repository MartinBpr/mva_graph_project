from igraph import *
import numpy as np
import matplotlib.pyplot as plt
import os.path
from scipy.optimize import curve_fit

################################################################
################################################################
#                      Parameters setting                      #
################################################################
################################################################

n_min = 30
n_max = 31
m_min = 1
m_max = 2

n_launch = 100000


################################################################
################################################################
#   Computation of alpha for many values of n, m and launches  #
################################################################
################################################################

degrees = {}

for n in range(n_min, n_max):
    degrees[n] = {}
    print n
    for m in range(m_min, m_max):
        for _ in range(n_launch):
            g = Graph.Barabasi(n, m, directed=False, outpref=True)
            if m in degrees[n]:
                degrees[n][m] += np.array(g.vs.degree())
            else:
                degrees[n][m] = np.array([float(d) for d in g.vs.degree()])

        degrees[n][m] /= n_launch

for i,v in enumerate(g.vs):
    v["label"] = i

#plot(g)

inspected =  degrees[n_min][1]
print inspected, sum(inspected)

#print [inspected[i+1]/inspected[i] for i in range(len(inspected) - 1)]
#plt.plot([inspected[i+1]/inspected[i] for i in range(len(inspected) - 1)])
#plt.show()