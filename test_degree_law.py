import igraph as ig
import numpy as np
import os.path
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from scipy.optimize import curve_fit




import tools

lib = 'nx'

n = 30
m = 6

n_launch = 100001


data_path = 'data/'
degree_name = data_path + 'degree_n_%i_m_%ix%i_%s' % (n, m, n_launch, lib)
edge_name = data_path + 'edges_n_%i_m_%ix%i_%s' % (n, m, n_launch, lib)

if os.path.isfile(degree_name + '.npy') and os.path.isfile(edge_name + '.npy'):
    degrees = np.load(degree_name + '.npy')
    edges = np.load(edge_name + '.npy')

# Otherwise we launch the estimation
else:
    edges = np.zeros((n,n))
    degrees = np.zeros(n)

    for ite in xrange(n_launch):
        if ite % 5000 == 0:
            print "%i/%i" % (ite, n_launch)
        if lib == 'ig':
            g = ig.Graph.Barabasi(n, m, outpref=True)
            for e in g.es:
                edges[e.source, e.target] += 1
            degrees += np.array(g.degree())
        elif lib == 'nx':
            G = tools.barabasi_albert_graph(n, m)
            for e in G.edges():
                edges[e[0], e[1]] += 1
            g_degree = G.degree()
            degrees += np.array([g_degree[i] for i in xrange(n)])
        else:
            raise Exception('unknown library')
        
    degrees /= n_launch
    edges /= n_launch

    np.save(degree_name, degrees)
    np.save(edge_name, edges)


#print edges

biased_law = np.zeros((n,n))

np.set_printoptions(precision=3)

reason = np.array([1.0 + 1.0/(2.0*i - (m+1)) if i > m else 0 for i in range(n)])

degrees_law = np.zeros((n, n + 1))
for i in xrange(0, m + 1):
    for t in range(m+1, n + 1):
        degrees_law[i, t] = m * np.prod(reason[m+1:t])

for i in xrange(m + 1, n + 1):
    for t in range(i+1, n + 1):
        degrees_law[i, t] = m * np.prod(reason[i+1:t])

print "empirical degrees\t", degrees
print "law degree\t\t\t", degrees_law[:,-1]

biased_law = np.zeros((n, n + 1))
for t in xrange(n + 1):
    for i in xrange(t):
        if t <= m and i <= m:
            biased_law[i, t] = 1
        else:
            biased_law[i, t] = 1.0 / (2*t - (m+1)) * degrees_law[i, t]

print "\n empirical probabilities\n", edges
print "\n computed probabilities\n", biased_law[:,: -1]

rel_err = np.array(edges)
rel_err[edges > 0] = (edges[edges > 0] - biased_law[edges > 0]) / edges[edges > 0]

tot_err = np.array(edges)
tot_err[edges > 0] = (edges[edges > 0] - biased_law[edges > 0])

print "\nerror\n", rel_err

ax1 = plt.subplot(1, 2, 1)
ax1.set_title("degree error")
ax2 = plt.subplot(2, 2, 2)
ax2.set_title("total error")
ax3 = plt.subplot(2, 2, 4)
ax3.set_title("Relative error")

ax1.plot(degrees - degrees_law[:,-1])
ax1.plot((degrees - degrees_law[:,-1]) / degrees)

fontP = FontProperties()
fontP.set_size('small')

for i in range(0, n):
    y2_axis = tot_err[i, :]
    y2_axis = y2_axis[edges[i, :] > 0]
    x2_axis = np.arange(n)[edges[i, :] > 0]
    ax2.plot(x2_axis, y2_axis)

    y3_axis = rel_err[i, :]
    y3_axis = y3_axis[edges[i, :] > 0]
    x3_axis = np.arange(n)[edges[i, :] > 0]
    ax3.plot(x3_axis, y3_axis, label="node %i" % i)

plt.legend(loc=6, ncol=3, prop=fontP, bbox_to_anchor=(-0.5, 1.))
plt.show()


avg_edges_error_on_big_degrees = sum(rel_err[:m+1, :]) / (m+1)
# let's keep only the one that are not deterministic
avg_edges_error_on_big_degrees = avg_edges_error_on_big_degrees[m+1:]

print avg_edges_error_on_big_degrees

def power_fit(x, a, b, c):
    default_value = 10
    abscisse = x - c
    if abscisse.__class__ == np.array([]).__class__:
        abscisse[abscisse < 0] = default_value
    return np.power(abscisse, a) + b

x = np.arange(len(avg_edges_error_on_big_degrees))
pow_fitting_parameters, covariance = curve_fit(power_fit, x, avg_edges_error_on_big_degrees)
a_s, b_s, c_s = pow_fitting_parameters
std_deviation = np.sqrt(np.diag(covariance))

s = ""
s += "avg error (t) = (t + c)^a + b\n"
s += "with\ta = %f (std = %f)\n" % (a_s, std_deviation[0])
s += "\t\tb = %f (std = %f)\n" % (b_s, std_deviation[1])
s += "\t\tc = %f (std = %f)\n" % (c_s, std_deviation[2])
print s

# plot the fitting
ax = plt.subplot(111)
trend_pow = power_fit(x, a_s, b_s, c_s)
ax.plot(x, avg_edges_error_on_big_degrees, ls='_', marker='o')
ax.plot(x, trend_pow, label='power trend')
ax.legend()
ax.set_xlabel("t")
ax.set_ylabel("avg error (t)")

#plt.show()

"""
correction = np.zeros(n + 1)
correction[m + 2:] = trend_pow
biased_law_corrected = np.zeros((n, n + 1))

for i in xrange(n):
    if i <= m:
        biased_law_corrected[i, :] = biased_law[i, :] - correction
    else:
        n_benefit_nodes = np.arange(n + 1) + 1 - (m + 1)
        biased_law_corrected[i, :] = biased_law[i, :] + correction * np.divide((m + 1), (np.arange(n + 1) + 1 - (m + 1)))

print sum(biased_law)
print sum(biased_law_corrected)
"""
