# -*- coding: utf-8 -*-


import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import igraph as ig
import networkx as nx
import tools
np.set_printoptions(precision=3)

import pandas as pd

print tools.get_empirical_law(4, 1, n_launch=5)[1]
print tools.get_biased_law(4, 1)[1]

"""
np.random.seed(3)
print np.random.random(5)
print np.random.random(5)
print np.random.random(5)
"""
"""
d = 20
T = 1000
def normalize_loss(losses):
    return (losses - min(losses))/(max(losses) - min(losses))

sigma2 = 1

def compute_losses_rw(rw):
    rw = np.random.normal(rw, sigma2)
    return normalize_loss(rw), rw

losses = np.zeros(d)
rw = np.zeros(d)
l = []
for t in range(T):
    l.append(list(losses))
    losses, rw = compute_losses_rw(rw)

l = np.array(l)
print l
for i in range(d):
    plt.plot(l[:, i])
plt.show()
"""

"""
data = pd.DataFrame(np.random.random((20,5)))

print data[:10].to_string(formatters=[lambda s : "%.3f" % s for c in data.columns])
"""
"""
n = 6
biased_law = tools.get_biased_law(n, 2)
biased_law = np.concatenate((biased_law, np.zeros((1, n + 1))), axis=0)
biased_law = biased_law + biased_law.T
biased_law = biased_law + np.eye(n+1)

print biased_law




"""

"""
g = ig.Graph(4)

g.add_edges(zip([0,0], [2,3]))

g = tools.barabasi_albert_graph_ig(6, 2)

ig.plot(g)
"""

"""
m = 4
G=nx.complete_graph(m)

nx.draw(G)
plt.show()

targets=list(range(m))
print targets
# List of existing nodes, with nodes repeated once for each adjacent edge

repeated_nodes=[]
for i in range(m-1):
    repeated_nodes.extend(targets)
print repeated_nodes
"""

"""
g = ig.Graph()
g.add_vertex()
g.add_vertex()
g.add_edge(0, 1)

for t in range(2,10):
    g.add_vertex()
    g.add_edge(0, t)
    if 0:
        g.add_edge(1, t)
    else:
        g.add_edge(t-1, t)


for i, v in enumerate(g.vs):
    v["label"] = i

#ig.plot(g)
print g.degree(), sum(g.degree()) - np.array(g.degree())

for e in g.es:
    print e.source, e.target
"""

"""
G = nx.path_graph(5)
nx.draw(G)
print nx.maximal_independent_set(G)
print len(nx.maximal_independent_set(G))
plt.show()
"""

"""
n = 4
m = 3

G = tools.barabasi_albert_graph(n, m)

nx.draw(G)
plt.show()
"""

"""
n = 3
m = 1

n_launch = 1

degrees = np.zeros(n)

for _ in xrange(n_launch):
    G = ig.Graph.Barabasi(n, m, outpref=True)
    degrees += G.degree()

print "igraph average degree of nodes 0, 1, 2\t\t", degrees/n_launch
"""

"""
degrees = np.zeros(n)

for _ in xrange(n_launch):
    G = nx.barabasi_albert_graph(n, m)
    g_degree = G.degree()
    degrees += np.array([g_degree[i] for i in xrange(n)])
    print G.edges()

print "networkx average degree of nodes 0, 1, 2\t", degrees/n_launch
"""
"""
"""
"""
n = 2
m = 1

g = ig.Graph.Barabasi(n, m, directed=False)
print g.vs.degree()

ig.plot(g)
"""
"""
print np.random.normal(np.array([3, 10]), 1)
"""
"""
np.random.seed(1234)

x = np.arange(10)
noise = 2*np.random.random(10) - 1

a = 0.7
b = 1.2

real_trend = a*x + b
y = real_trend + noise


def affine_fit(x, slope, y_at_origin):
    return slope * x + y_at_origin

(a_, b_), covariance = curve_fit(affine_fit, x, y)

estimated_trend = a_ * x + b_

plt.plot(x, real_trend, label="real")
plt.plot(x, y, linestyle="_", marker="o", label="values")
plt.plot(x, estimated_trend, linestyle="--", label="estimated")

plt.legend(loc=4)
plt.show()
"""


"""
r = np.arange(10)

print r

print np.power(2, r)


log_factorial_t = [np.log(1)]#scipy.misc.factorial(np.arange(T))
for t in range(1, 200):
    log_factorial_t.append(log_factorial_t[t-1] + np.log(t+1))

print log_factorial_t[:100] - np.log(scipy.misc.factorial(np.arange(1,101)))
"""
