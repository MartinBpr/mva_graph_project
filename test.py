from igraph import *
import numpy as np
import matplotlib.pyplot as plt
import os.path

n_min = 6
n_max = 50
m_min = 1
m_max = 30

n_launch = 1000

alpha_name = 'alphas_n_%i-%i_m_%i-%i_x%i' % (n_min, n_max, m_min, m_max, n_launch)

if os.path.isfile(alpha_name + '.npy'):
    alphas = np.load(alpha_name + '.npy')
else:
    alphas = np.zeros((m_max - m_min, n_max - n_min))

    for n in range(n_min, n_max):
        print n
        for m in range(m_min, min(m_max, n - 3)):
            for _ in range(n_launch):
                g = Graph.Barabasi(n, m)
                alpha = g.independence_number()
                alphas[m - m_min, n - n_min] += alpha

    alphas /= n_launch
    np.save(alpha_name, alphas)




m_s = []


for m in range(m_min, m_max):
    y = alphas[m - m_min,:]

    x = np.arange(n_min,n_max)

    x = x[y > 2]
    A = np.vstack([x, np.ones(len(x))]).T
    y = y[y > 2]
    #print y
    m, c = np.linalg.lstsq(A, y)[0]
    m_s += [m]
    #print m, c

from scipy.optimize import curve_fit

def power_fit(x, a, b, c):
    return np.power( (x - c), a) + b

def exp_fit(x, a, b, c):
    return np.exp(a * (x - c)) + b

def log_fit(x, a, b, c):
    return np.log(-a * (x - c)) + b

x = np.array(range(m_min, m_max))
y = np.array(m_s)

pow_fitting_parameters, covariance = curve_fit(power_fit, x, y)
a_pow, b_pow, c_pow = pow_fitting_parameters
print "pow", pow_fitting_parameters, 'cov', covariance
trend_pow = power_fit(x, a_pow, b_pow, c_pow)
guessed_trend = power_fit(x, -1.0, 0.5, -3.0)
'''
exp_fitting_parameters, covariance = curve_fit(exp_fit, x, y)
a_exp, b_exp, c_exp = exp_fitting_parameters
print "exp", exp_fitting_parameters
trend_exp = exp_fit(x, a_exp, b_exp, c_exp)

log_fitting_parameters, covariance = curve_fit(log_fit, x, y)
a_log, b_log, c_log = log_fitting_parameters
print "log", log_fitting_parameters
trend_log = log_fit(x, a_log, b_log, c_log)
'''
plt.plot(x, y, ls = '_', marker = 'o')
plt.plot(x, trend_pow, label='power trend')
plt.plot(x, guessed_trend, label='guessed power trend')
#plt.plot(x, trend_exp, label='exp trend')
#plt.plot(x, trend_log, label='log trend')

plt.legend()
#plt.plot(np.append(y, next_y), 'ro')
plt.show()



law = np.zeros((m_max - m_min, n_max - n_min))
for n in range(n_min, n_max):
    for m in range(m_min, m_max):
        coef_dir = power_fit(m, a_pow, b_pow, c_pow)
        law[m - m_min, n - n_min] = (max(n-m, 0) + 1)*coef_dir



#plt.plot(range(m_min, m_max), m_s)
#plt.show()
#print alphas[:, -1]
#print alphas[:, 0]

'''
print alphas
ax1 = plt.subplot(211)
ax1.matshow(alphas)
#plt.colorbar()

ax2 = plt.subplot(212)



ax2.matshow(law)
print law

plt.show()
'''


linestyles = ['-', '--']
colors = ('b', 'g', 'r', 'c', 'm', 'y', 'k')

ax = plt.subplot(111)
for m in range(m_min, m_max):
    ax.plot(range(n_min, n_max), alphas[m-m_min,:], c=colors[(m - m_min) % len(colors)], ls = linestyles[0],\
             label = 'm=%i' % m, marker='o')
    ax.plot(law[m-m_min,:], c=colors[(m - m_min) % len(colors)], ls = linestyles[1])

ax.set_xlabel('n')
ax.set_ylabel('alpha')

ax.legend(loc=2)
plt.show()

