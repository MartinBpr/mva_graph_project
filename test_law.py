from igraph import *
import numpy as np
import matplotlib.pyplot as plt
import os.path

n_min = 6
n_max = 20
m_min = 1
m_max = 10

n_launch = 1000

################################################################
################################################################
#   Computation of alpha for many values of n, m and launches  #
################################################################
################################################################

# We look if this combination of n, m dans n_launch has not already be computed
data_path = 'data/'
alpha_name = data_path + 'alphas_n_%i-%i_m_%i-%i_x%i' % (n_min, n_max, m_min, m_max, n_launch)

if os.path.isfile(alpha_name + '.npy'):
    alphas = np.load(alpha_name + '.npy')

# Otherwise we launch the estimation
else:
    alphas = np.zeros((m_max - m_min, n_max - n_min))

    for n in range(n_min, n_max):
        print n
        # we make m stop at n - 3 because we observe an affine trend only until m < n - 3
        for m in range(m_min, min(m_max, n - 3)):
            for _ in range(n_launch):
                g = Graph.Barabasi(n, m, directed=False)
                alpha = g.independence_number()
                alphas[m - m_min, n - n_min] += alpha

    alphas /= n_launch
    np.save(alpha_name, alphas)

################################################################
################################################################
#                        estimate law                          #
################################################################
################################################################

def a_m(m):
    return np.power(m + 2.86053683,  -1.05328046) + 0.45690876

def b_m(m):
    return -0.44893736 * m - 0.0687451

law = np.zeros((m_max - m_min, n_max - n_min))
for n in range(n_min, n_max):
    for m in range(m_min, min(m_max, n - 3)):
        law[m - m_min, n - n_min] = a_m(m) * n + b_m(m)


################################################################
################################################################
#                   analyse relative errors                    #
################################################################
################################################################

errors = np.abs(alphas - law)
errors[alphas != 0] = np.divide(errors[alphas != 0], alphas[ alphas != 0])

positions = range(n_max - n_min)
alpha = ["%i" % n for n in range(n_min, n_max)]

data = np.random.random((4,4))
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(errors, interpolation='nearest')

fig.colorbar(cax)

plt.xticks(positions, alpha)

plt.show()
