# -*- coding: utf-8 -*-


import numpy as np
import networkx as nx
import igraph as ig
import os

def _random_subset(seq,m):
    """ Return m unique elements from seq.

    This differs from random.sample which can return repeated
    elements if seq holds repeated elements.
    """
    targets=set()
    while len(targets)<m:
        x= np.random.choice(seq)
        targets.add(x)
    return targets

def barabasi_albert_graph(n, m, seed=None):
    """Return random graph using Barabási-Albert preferential attachment model.

    A graph of n nodes is grown by attaching new nodes each with m
    edges that are preferentially attached to existing nodes with high
    degree.

    Parameters
    ----------
    n : int
        Number of nodes
    m : int
        Number of edges to attach from a new node to existing nodes
    seed : int, optional
        Seed for random number generator (default=None).

    Returns
    -------
    G : Graph

    Notes
    -----
    The initialization is a graph with with m nodes and no edges.

    References
    ----------
    .. [1] A. L. Barabási and R. Albert "Emergence of scaling in
       random networks", Science 286, pp 509-512, 1999.
    """

    if m < 1 or  m >=n:
        raise nx.NetworkXError(\
              "Barabási-Albert network must have m>=1 and m<n, m=%d,n=%d"%(m,n))

    # Add m initial nodes (m0 in barabasi-speak)
    G=nx.complete_graph(m)
    G.name="barabasi_albert_graph(%s,%s)"%(n,m)
    # Target nodes for new edges
    targets=list(range(m))
    # List of existing nodes, with nodes repeated once for each adjacent edge
    repeated_nodes=[]
    for i in range(m-1):
        repeated_nodes.extend(targets)
    # Start adding the other n-m nodes. The first node is m.
    source=m
    while source<n:
        # Add edges to m nodes from the source.
        G.add_edges_from(zip([source]*m,targets))
        # Add one node to the list for each new edge just created.
        repeated_nodes.extend(targets)
        # And the new node "source" has m edges to add to the list.
        repeated_nodes.extend([source]*m)
        # Now choose m unique nodes from the existing nodes
        # Pick uniformly from repeated_nodes (preferential attachement)
        targets = _random_subset(repeated_nodes,m)
        source += 1
    return G


def barabasi_albert_graph_ig(n, m, seed=None):
    """Return random graph using Barabási-Albert preferential attachment model.

    A graph of n nodes is grown by attaching new nodes each with m
    edges that are preferentially attached to existing nodes with high
    degree.

    Parameters
    ----------
    n : int
        Number of nodes
    m : int
        Number of edges to attach from a new node to existing nodes
    seed : int, optional
        Seed for random number generator (default=None).

    Returns
    -------
    G : Graph

    Notes
    -----
    The initialization is a graph with with m nodes and no edges.

    References
    ----------
    .. [1] A. L. Barabási and R. Albert "Emergence of scaling in
       random networks", Science 286, pp 509-512, 1999.
    """

    if m < 1 or  m >=n:
        raise nx.NetworkXError(\
              "Barabási-Albert network must have m>=1 and m<n, m=%d,n=%d"%(m,n))

    # Add m initial nodes (m0 in barabasi-speak)
    g = ig.Graph.Full(m)
    # Target nodes for new edges
    targets = list(range(m))
    # List of existing nodes, with nodes repeated once for each adjacent edge
    repeated_nodes = []
    for i in range(m-1):
        repeated_nodes.extend(targets)
    # Start adding the other n-m nodes. The first node is m.
    source = m
    while source<n:
        # Add edges to m nodes from the source.
        g.add_vertex()
        g.add_edges(zip([source]*m,targets))
        # Add one node to the list for each new edge just created.
        repeated_nodes.extend(targets)
        # And the new node "source" has m edges to add to the list.
        repeated_nodes.extend([source]*m)
        # Now choose m unique nodes from the existing nodes
        # Pick uniformly from repeated_nodes (preferential attachement)
        targets = _random_subset(repeated_nodes,m)
        source += 1
    return g


def get_biased_law(n, m):
    reason = np.array([1.0 + 1.0/(2.0*i - (m+1)) if i > m else 0 for i in range(n)])

    degrees_law = np.zeros((n, n + 1))
    for i in xrange(0, m + 1):
        for t in range(m+1, n + 1):
            degrees_law[i, t] = m * np.prod(reason[m+1:t])

    for i in xrange(m + 1, n):
        for t in range(i+1, n + 1):
            degrees_law[i, t] = m * np.prod(reason[i+1:t])

    biased_law = np.zeros((n, n))
    for t in xrange(n):
        for i in xrange(t):
            if t <= m and i <= m:
                biased_law[i, t] = 1
            else:
                biased_law[i, t] = 1.0 / (2*t - (m+1)) * degrees_law[i, t]

    return biased_law, degrees_law[:, -1]


def get_empirical_law(n, m, n_launch=100000):
    data_path = 'data/'
    degree_name = data_path + 'degree_n_%i_m_%ix%i_%s' % (n, m, n_launch, "cus")
    edge_name = data_path + 'edges_n_%i_m_%ix%i_%s' % (n, m, n_launch, "cus")

    if os.path.isfile(degree_name + '.npy') and os.path.isfile(edge_name + '.npy'):
        degrees = np.load(degree_name + '.npy')
        edges = np.load(edge_name + '.npy')

    # Otherwise we launch the estimation
    else:
        edges = np.zeros((n, n))
        degrees = np.zeros(n)

        for ite in xrange(n_launch):
            if ite % 5000 == 0:
                print "%i/%i" % (ite, n_launch)
            g = barabasi_albert_graph_ig(n, m)
            for e in g.es:
                edges[e.source, e.target] += 1
            degrees += np.array(g.degree())

        degrees /= n_launch
        edges /= n_launch

        np.save(degree_name, degrees)
        np.save(edge_name, edges)

    return edges, degrees


