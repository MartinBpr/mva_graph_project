import igraph as ig
import numpy as np
import os.path
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from scipy.optimize import curve_fit
from matplotlib.ticker import FuncFormatter
import matplotlib.ticker

import tools

lib = 'nx'

samples = [(30, 1), (30, 6)]

n_launch = 100000
fig = plt.figure()

def to_percent(x, y):
    return "%i" % int(100*x) + '%'

for sample, (n, m) in enumerate(samples):
    edges, _ = tools.get_empirical_law(n, m, n_launch=n_launch)

    edges_law, _ = tools.get_biased_law(n, m)

    errors = edges_law - edges
    errors[edges != 0] = np.divide(errors[edges != 0], edges[edges != 0])
    ax = fig.add_subplot(2, 2, sample+3)
    cax = ax.matshow(errors, interpolation='nearest', cmap=plt.get_cmap("bwr"), vmin=-0.10, vmax=0.10)
    cb = fig.colorbar(cax)
    cb.set_label("relative error")

    ax.set_title("error n=%i, m=%i" % (n,m))
    ax.set_ylabel("node index")
    ax.set_xlabel("node index")
    ax.xaxis.set_ticks_position('bottom')
    #cb.ax.yaxis.set_major_formatter(FuncFormatter(to_percent))
    cb.formatter   = FuncFormatter(to_percent)

    cb.update_ticks()

    ax = fig.add_subplot(2, 2, sample+1)
    cax = ax.matshow(edges, interpolation='nearest', cmap=plt.get_cmap("Reds"))
    cb = fig.colorbar(cax)
    cb.set_label("edge probability")
    ax.set_title("law to fit n=%i, m=%i" % (n,m))
    ax.set_ylabel("node index")
    ax.set_xlabel("node index")
    ax.xaxis.set_ticks_position('bottom')


plt.subplots_adjust(hspace=0.4)
plt.show()

