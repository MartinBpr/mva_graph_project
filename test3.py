from igraph import *

n = 10
m = 2

g = Graph.Barabasi(n, m, directed=True)

plot(g, arrow_size=3)