import igraph as ig
import numpy as np
import matplotlib.pyplot as plt
import os.path
from scipy.optimize import curve_fit
import tools

################################################################
################################################################
#                      Parameters setting                      #
################################################################
################################################################

n_min = 6
n_max = 35
m_min = 1
m_max = 20

n_launch = 1000


################################################################
################################################################
#   Computation of alpha for many values of n, m and launches  #
################################################################
################################################################

# We look if this combination of n, m dans n_launch has not already be computed
data_path = 'data/'
alpha_name = data_path + 'alphas_n_%i-%i_m_%i-%i_x%i' % (n_min, n_max, m_min, m_max, n_launch)

if os.path.isfile(alpha_name + '.npy'):
    alphas = np.load(alpha_name + '.npy')

# Otherwise we launch the estimation
else:
    alphas = np.zeros((m_max - m_min, n_max - n_min))

    for n in range(n_min, n_max):
        print n
        # we make m stop at n - 3 because we observe an affine trend only until m < n - 3
        for m in range(m_min, min(m_max, n - 3)):
            for _ in range(n_launch):
                #g = ig.Graph.Barabasi(n, m, outpref=True)
                g = tools.barabasi_albert_graph_ig(n, m)
                alpha = g.independence_number()
                alphas[m - m_min, n - n_min] += alpha

    alphas /= n_launch
    np.save(alpha_name, alphas)


################################################################
################################################################
#                    Parameters estimation                     #
################################################################
################################################################

####################################################
#  estimation of slope and y_at_origin such that   #
#    alpha(n,m) = slope(m)*n + y_at_origin(m)      #
####################################################

slopes = []
y_at_origins = []

# function of an affine curve
def affine_fit(x, slope, y_at_origin):
    return slope * x + y_at_origin

for m in range(m_min, m_max):
    y = alphas[m - m_min,:]
    x = np.arange(n_min, n_max)

    # we remove uncomputed y
    x = x[y > 0]
    y = y[y > 0]

    slope, y_at_origin = curve_fit(affine_fit, x, y)[0]
    slopes += [slope]
    y_at_origins += [y_at_origin]

    #plt.plot(x, y, ls="_", marker="o")
    #plt.plot(x, affine_fit(x, slope, y_at_origin) )
    #print slope

#print slopes

#plt.show()

s = ""
s += "alpha(m,n) = slope(m) * n + y_at_origin(m)\n"
s += "where\n"
print s


####################################################
# estimation of slope(m) as a power function of m  #
####################################################

# function of a power relationship
def power_fit(x, a, b, c):
    default_value = 10
    abscisse = x - c
    if abscisse.__class__ == np.array([]).__class__:
        abscisse[abscisse < 0] = default_value
    return np.power(abscisse, a) + b


x = np.array(range(m_min, m_max))
y = np.array(slopes)

pow_fitting_parameters, covariance = curve_fit(power_fit, x, y)
a_s, b_s, c_s = pow_fitting_parameters
std_deviation = np.sqrt(np.diag(covariance))

s = ""
s += "slope(m) = (m + c)^a + b\n"
s += "with\ta = %f (std = %f)\n" % (a_s, std_deviation[0])
s += "\t\tb = %f (std = %f)\n" % (b_s, std_deviation[1])
s += "\t\tc = %f (std = %f)\n" % (c_s, std_deviation[2])
print s

# plot the fitting
ax = plt.subplot(2, 2, 2)
trend_pow = power_fit(x, a_s, b_s, c_s)
ax.plot(x, y, ls='_', marker='o')
ax.plot(x, trend_pow, label='power trend')
ax.legend()
ax.set_xlabel("m")
ax.set_ylabel("slope(m)")
ax.set_title("fitting of the slope")

####################################################
#         estimation of y_at_origin(m)             #
#          as an affine function of m              #
####################################################

x = np.array(range(m_min, m_max))
y = np.array(y_at_origins)

lin_fitting_parameters, covariance = curve_fit(affine_fit, x, y)
a_y, b_y = lin_fitting_parameters
std_deviation = np.sqrt(np.diag(covariance))


# lin_fitting_parameters, covariance = curve_fit(affine_fit, x, y)
# a_lin, b_lin = lin_fitting_parameters
# std_deviation = np.sqrt(np.diag(covariance))

s = ""
s += "intercept(m) = a*m + b\n"
s += "with\ta = %f (std = %f)\n" % (a_y, std_deviation[0])
s += "\t\tb = %f (std = %f)\n" % (b_y, std_deviation[1])
print s

# plot the fitting
ax = plt.subplot(2, 2, 4)
trend_pow = affine_fit(x, a_y, b_y)
ax.set_xlabel("m")
ax.set_ylabel("intercept(m)")
ax.set_title("fitting of the intercept")
ax.plot(x, y, ls='_', marker='o')
ax.plot(x, trend_pow, label='affine trend')
ax.legend()

################################################################
################################################################
#        Plot alpha estimation for different m values          #
################################################################
################################################################

####################################################
#               law estimation                     #
####################################################

law = np.zeros((m_max - m_min, n_max - n_min))
for m in range(m_min, m_max):
    slope = power_fit(m, a_s, b_s, c_s)
    origin_y = affine_fit(m, a_y, b_y)
    for n in range(max(n_min, m+3), n_max):
        law[m - m_min, n - n_min] = affine_fit(n, slope, origin_y)


####################################################
#                  law plot                        #
####################################################

line_styles = ['_', '-']
colors = ('b', 'g', 'r', 'c', 'm', 'y', 'k')

selected_m = range(m_min, m_max)
# we select only some of them of them
selected_m = [m for m in selected_m if m % 3 == 0]

ax = plt.subplot(1, 2, 1)
for m in selected_m:
    alpha_values = alphas[m-m_min,:]
    law_values = law[m-m_min,:]
    x_values = np.array(range(n_min, n_max))

    x_values = x_values[alpha_values > 0]
    law_values = law_values[alpha_values > 0]
    alpha_values = alpha_values[alpha_values > 0]

    ax.plot(x_values, alpha_values, c=colors[(m - m_min) % len(colors)], ls = line_styles[0],\
             label='m=%i' % m, marker='o')
    ax.plot(x_values, law_values, c=colors[(m - m_min) % len(colors)], ls = line_styles[1])

ax.set_xlabel('n')
ax.set_ylabel('alpha')
ax.legend(loc=2)
ax.set_title("Value of alpha and its estimation \ndepending on n for few values of m")
plt.subplots_adjust(hspace=0.4)
plt.subplots_adjust(wspace=0.33)

plt.show()


# colormap of law
fig = plt.figure()

ax1 = fig.add_subplot(211)
cax = ax1.matshow(alphas)

fig.colorbar(cax)


ax2 = plt.subplot(212)
ax2.matshow(law)

#plt.show()