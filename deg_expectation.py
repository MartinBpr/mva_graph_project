import numpy as np


m = 1

def total_degree_at_t(t):
    return 2 * m * (t)


def add_or_init(key, value, d):
    if key in d:
        d[key] += value
    else:
        d[key] = value

# expectation of degree of node 0 #
proba_at_step = {}
proba_at_step[1] = {1:1.0}

det = 1

expectations = {1:1.0}

for t in range(2, 31):
    proba_at_step[t] = {}
    for k in proba_at_step[t-1].keys():
        det *= total_degree_at_t(t - 1)

        prob_add_edge = float(k) / total_degree_at_t(t - 1)
        add_or_init(k+1, prob_add_edge * proba_at_step[t-1][k], proba_at_step[t])

        prob_stay = 1.0 - prob_add_edge
        add_or_init(k, prob_stay * proba_at_step[t-1][k], proba_at_step[t])

    expectations[t] = sum([ key * value for key,value in proba_at_step[t].iteritems()])

    print t, expectations[t], expectations[t]/expectations[t-1], 1 + 1.0/(2*(t-1))
    #print t, np.log(expectation * det), "%i / %i" % (expectation * det, det) , proba_at_step[t]

# expectation of degree of node 2 #

proba_at_step = {}
proba_at_step[2] = {1:1.0}

expectations = {2:1.0}

print "\n\n NODE 2"

for t in range(3, 1000):
    proba_at_step[t] = {}
    for k in proba_at_step[t-1].keys():
        det *= total_degree_at_t(t - 1)
        prob_add_edge = float(k) / total_degree_at_t(t - 1)
        add_or_init(k+1, prob_add_edge * proba_at_step[t-1][k], proba_at_step[t])

        prob_stay = 1.0 - prob_add_edge
        add_or_init(k, prob_stay * proba_at_step[t-1][k], proba_at_step[t])

    expectations[t] = sum([ key * value for key,value in proba_at_step[t].iteritems()])

    tmp = [1.0 + 1.0/(2.0*i) for i in range(2,t)]

    print t, expectations[t], np.prod(tmp), 1*np.sqrt(float(t)/2.0), np.prod(tmp) / (1*np.sqrt(float(t)/2.0))#, expectations[t]/expectations[t-1], 1 + 1.0/(2*(t-1))
