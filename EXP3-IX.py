import igraph as ig
import random
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import tools
from matplotlib.font_manager import FontProperties

random.seed(1240)
np.random.seed(8)

################################################################
################################################################
#                        Create Graph                          #
################################################################
################################################################

d = 30
m = 6

o_estimations = []

o_estimations += ['with empirical estimation']
o_estimations += ['with graph observation']
o_estimations += ['with biased estimation']

#loss_type = "static"
loss_type = "rw"


# Give one (static atm) loss to each node
mean_loss = np.random.rand(d) * 3 + 1


################################################################
################################################################
#                         Algorithm                            #
################################################################
################################################################


####################################################
#                  Needed functions                #
####################################################

def compute_o(p, observed_nodes):
    return np.array([sum([p[v_j.index] for v_j in g.vs[v_i].neighbors(mode="IN")]) for v_i in observed_nodes])


#biased_law = tools.get_biased_law(d - 1, m)
#biased_law = np.concatenate((biased_law, np.zeros((1, d))), axis=0)

biased_law, _ = tools.get_biased_law(d, m)
biased_law = biased_law + biased_law.T
biased_law = biased_law + np.eye(d)
def compute_o_biased_law(p, observed_nodes):
    return [np.sum( np.multiply( biased_law[v_i, :], p ) ) for v_i in observed_nodes]


empirical_law, _ = tools.get_empirical_law(d, m, n_launch=100000)
empirical_law += empirical_law.T
empirical_law += np.eye(d)
def compute_o_empirical_law(p, observed_nodes):
    return [np.sum( np.multiply( empirical_law[v_i, :], p ) ) for v_i in observed_nodes]


# in order to have l_i \in [0,1]
def normalize_loss(losses):
    return (losses - min(losses))/(max(losses) - min(losses))

sigma2 = 1
def compute_losses(mean_loss):
    losses = np.random.normal(mean_loss, sigma2)
    return normalize_loss(losses)


def compute_losses_rw(rw):
    rw = np.random.normal(rw, sigma2)
    return normalize_loss(rw), rw


def compute_gamma(t, Q):
    sum_Q = np.sum(Q)
    return np.sqrt(np.log(d)/(d + sum_Q))

####################################################
#                  Initialisation                #
####################################################

dict_L_ = {compute_o_method: np.zeros(d) for compute_o_method in o_estimations}
T = 12000
dict_regret = {compute_o_method: np.zeros(T + 1) for compute_o_method in o_estimations}
dict_Q = {compute_o_method: np.zeros(T) for compute_o_method in o_estimations}
dict_H = {compute_o_method: np.zeros(T) for compute_o_method in o_estimations}
dict_alpha = {compute_o_method: np.zeros(T) for compute_o_method in o_estimations}
dict_regret_bound = {compute_o_method: np.zeros(T) for compute_o_method in o_estimations}
rw = np.zeros(d)
best_loss_change = []

# We first use a static graph because computing the exact independence number is expensive
alpha_t = 9.6# g.independence_number()
print "Independence number of graph %i\n" % alpha_t


####################################################
#                  Launch Algorithm                #
####################################################

print "Algorithm launched"
for t in xrange(T):
    if t % 2000 == 0 :
        print "iteration : %i/%i" % (t, T)


    g = ig.Graph.Barabasi(d, m, directed=False, outpref=True)

    # Add self-loop to have a strongly observable graph
    for v in g.vs:
        g.add_edge(v, v)

    if loss_type == "static":
        chosen_losses = compute_losses(mean_loss) #5
    elif loss_type == "rw":
        chosen_losses, rw = compute_losses_rw(rw) #5
        previous_losses = chosen_losses

    for compute_o_method in o_estimations:
        L_ = dict_L_[compute_o_method]
        regret = dict_regret[compute_o_method]
        Q = dict_Q[compute_o_method]
        H = dict_H[compute_o_method]
        alpha = dict_alpha[compute_o_method]
        regret_bound = dict_regret_bound[compute_o_method]

        gamma = compute_gamma(t, Q)
        eta = gamma

        # Run algorithm
        w = 1.0/d * np.exp(- eta * L_) #4

        W = sum(w) #6
        p = w/W #7
        I = np.random.multinomial(1, p)
        I = np.nonzero(I)[0][0] #8
        observed_nodes = [v.index for v in g.vs[I].neighbors(mode="OUT")]
        observed_losses = np.array([chosen_losses[i] for i in observed_nodes]) #10
        if compute_o_method == 'with graph observation':
            o_observed_nodes = compute_o(p, observed_nodes) #11
        elif compute_o_method == 'with biased estimation':
            o_observed_nodes = compute_o_biased_law(p, observed_nodes) #11
        elif compute_o_method == 'with empirical estimation':
            o_observed_nodes = compute_o_empirical_law(p, observed_nodes) #11
        estimated_losses = np.divide(observed_losses, o_observed_nodes + gamma) #12

        # Update L and compute regret
        L_[observed_nodes] += estimated_losses

        # compute regret and theoretical bounds

        if t > 0:
            if np.argmin(chosen_losses) != last_best_loss:
                best_loss_change += [t]
        last_best_loss = np.argmin(chosen_losses)

        min_loss = min(chosen_losses)
        paid_loss = np.sum(np.multiply(chosen_losses, p))

        regret[t+1] = regret[t] + (paid_loss - min_loss)

        if compute_o_method == 'with graph observation':
            o_all = compute_o(p, xrange(d))
        elif compute_o_method == 'with biased estimation':
            o_all = compute_o_biased_law(p, xrange(d))
        elif compute_o_method == 'with empirical estimation':
            o_all = compute_o_empirical_law(p, xrange(d))
        Q[t] = np.sum(np.divide(p, o_all + gamma))
        alpha[t] = alpha_t

        H[t] = np.log(1 + (np.floor(d*d * np.sqrt( float(t*d) / np.log(d))) + d + 1) / alpha_t)
        regret_bound[t] = 4 * np.sqrt((d + 2 * np.sum(np.multiply(H, alpha) + 1)) * np.log(d))


################################################################
################################################################
#                      Analyse results                         #
################################################################
################################################################

####################################################
#             proba - loss relationship            #
####################################################

results = pd.DataFrame()
results["loss"] = mean_loss
for compute_o_method in o_estimations:
    w = 1.0/d * np.exp(- eta * dict_L_[compute_o_method]) #4
    W = sum(w) #6
    p = w/W #7
    results["%s" % compute_o_method] = p
print "\nLoss and probability of playing each node"
results = results.sort(columns="loss", ascending=True)
print results[:10].to_string(formatters=[lambda s : "%.5f" % s for c in results.columns])



####################################################
#          Regret and theoretical bound            #
####################################################
fontP = FontProperties()
fontP.set_size('small')

ax = plt.subplot(121)


for compute_o_method in o_estimations:
    ax.plot(dict_regret[compute_o_method], label="regret %s" % compute_o_method)

ax.plot(dict_regret_bound[compute_o_method], label="theoretical regret bound")

ax.legend(loc=2)#, prop=fontP)
ax.set_title("regret vs. theoretical bound")
ax.set_ylabel("regret")
ax.set_xlabel("iteration")

ax = plt.subplot(122)


for t in best_loss_change:
    ax.axvline(t, color='cyan', linestyle='solid', alpha=0.2)

for compute_o_method in o_estimations:
    ax.plot(dict_regret[compute_o_method], label="regret %s" % compute_o_method)

ax.set_title("regret and best action switch")
ax.set_ylabel("regret")
ax.set_xlabel("iteration")

ax.legend(loc=2)#, prop=fontP)


#log_d_power_t = np.arange(T) * np.log(d)
#log_factorial_t = [np.log(1)]
#for t in range(1, T):
#    log_factorial_t.append(log_factorial_t[t-1] + np.log(t+1))

#ax.plot( np.sqrt( log_d_power_t + log_factorial_t))
#ax.set_title("sqrt( log ( d^t * t! ) )")

plt.show()
